library(tidyverse)

read_csv("summary.csv")-> s
ggplot(s, aes(x=db_engine,y=rate)) +
  scale_y_continuous(expand=c(0,0), breaks = scales::pretty_breaks(n = 10))+
  geom_bar(stat='identity',fill="#00BFC4", color="black") +
  coord_flip() +
  facet_wrap(~operation, scales = "free", dir="v") +
  labs(
    x="Metadata Engine",
    y="Object/sec",
    caption="Get the code to reproduce this graph at https://git.deuxfleurs.fr/Deuxfleurs/mknet",
    title="Comparison of Garage's metadata engines with \"minio/warp\"",
    subtitle="Daemon: Garage v0.8 no-fsync to avoid being impacted by block manager\nBenchmark: warp, mixed mode, 5min bench, 256B objects, initialized with 200 objects.\nEnvironment: mknet (Ryzen 5 1400, 16GB RAM, SSD). DC topo (3 nodes, 1Gb/s, 1ms latency).",
  ) +
  theme_classic()
ggsave("./plot.png", width=200, height=120, units="mm")
