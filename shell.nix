{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    nativeBuildInputs = [ 
      pkgs.python310
      pkgs.python310Packages.pyyaml
      pkgs.python310Packages.requests

      # to test the pip setup
      pkgs.python310Packages.pip
      pkgs.python310Packages.setuptools
    ];
}
