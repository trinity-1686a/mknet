module git.deuxfleurs.fr/Deuxfleurs/mknet/benchmarks/s3ttfb

go 1.16

require (
	github.com/aws/aws-sdk-go-v2 v1.16.16
	github.com/aws/aws-sdk-go-v2/config v1.17.7
	github.com/aws/aws-sdk-go-v2/service/s3 v1.27.11
	github.com/google/uuid v1.1.1
	github.com/minio/minio-go/v7 v7.0.16
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
)
