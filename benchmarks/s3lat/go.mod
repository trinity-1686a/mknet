module git.deuxfleurs.fr/quentin/s3lat

go 1.16

require (
	github.com/google/uuid v1.1.1
	github.com/minio/minio-go/v7 v7.0.16
)
