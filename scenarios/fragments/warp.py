import os
from os.path import exists
from pathlib import Path
from fragments import shared, garage, flavor, minio

warp_bin = Path(os.path.dirname(__file__)) / "../../benchmarks/warp/warp"
bench = flavor.warp["warp-fast"]

def common(port, ak, sk):
    out = Path(shared.storage_path) / "warp.csv"
    shared.log(f"launching warp {warp_bin}")
    shared.exec(f"{warp_bin} {bench} --host=[{os.environ['IP']}]:{port} --analyze.out={out} --access-key={ak} --secret-key={sk}")
    shared.log(f"execution done")

def on_garage(): common(3900, garage.key.access_key_id, garage.key.secret_access_key)
def on_minio(): common(9000, minio.access_key, minio.secret_key)

