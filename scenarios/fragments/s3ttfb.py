import os
from os.path import exists
from pathlib import Path
from fragments import shared, minio, garage

s3bin = Path(os.path.dirname(__file__)) / "../../benchmarks/s3ttfb/s3ttfb"

def common():
    out = Path(shared.storage_path) / "s3ttfb.csv"
    shared.log(f"launching s3ttfb ({s3bin})")
    shared.exec(f"{s3bin} > {out}")
    shared.log(f"execution done, output written to {out}")

def on_garage():
    os.environ['AWS_ACCESS_KEY_ID'] = garage.key.access_key_id
    os.environ['AWS_SECRET_ACCESS_KEY'] = garage.key.secret_access_key
    os.environ['ENDPOINT'] = "localhost:3900"
    os.environ['AWS_REGION'] = "garage"
    common()

def on_minio():
    os.environ['AWS_ACCESS_KEY_ID'] = minio.access_key
    os.environ['AWS_SECRET_ACCESS_KEY'] = minio.secret_key
    os.environ['ENDPOINT'] = "localhost:9000"
    os.environ['AWS_REGION'] = "us-east-1"
    common()
