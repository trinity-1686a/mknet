from pathlib import Path
from . import shared
from os.path import exists

def grg_path(d):
    for flav, desc in d.items():
        if "path" in desc: continue
        binary = f"garage-{desc['target']}-{desc['version']}"
        desc['path'] = Path(shared.binary_path) / binary
    return d

def minio_path(d):
    for flav, desc in d.items():
        if "path" in desc: continue
        desc['path'] = Path(shared.binary_path) / flav
    return d

garage = grg_path({
    "garage-local": { "path": "./garage/target/release/garage" },
    "garage-v0.7": { 
        "version": "v0.7.3", 
        "target": "x86_64-unknown-linux-musl", 
    },
    # no rpc optimization
    "garage-v0.8-beta1": { 
        "version": "89b8087ba81c508ba382aa6c9cb6bb3afa6a43c8", 
        "target": "x86_64-unknown-linux-musl" 
    },
    # with rpc optimizations
    # with fsync deactivated
    "garage-v0.8-no-fsync": { 
        "version": "v0.8.0-dangerous-no-fsync", 
        "target": "x86_64-unknown-linux-musl" 
    },
    # with rpc optimizations
    "garage-v0.8-beta2": {
        "version": "af2b2f26b4bd9fcdeedf2cd03f9e1392e5781abc",
        "target": "x86_64-unknown-linux-musl",
    }
})

warp = {
    "warp-fast": "mixed --obj.size 5M --objects 200 --duration=5m",
    "warp-small-obj": "mixed --obj.size 256 --objects 200 --duration=5m",
    "warp-default": "mixed"
}

minio = minio_path({
    "minio-20220917": {
        "version": "2022-09-17T00-09-45Z"
    }
})


def download():
    for flav, desc in garage.items():
        if "version" not in desc: continue
        if exists(desc['path']): continue

        shared.exec(f"mkdir -p {shared.binary_path}")
        shared.exec(f"wget https://garagehq.deuxfleurs.fr/_releases/{desc['version']}/{desc['target']}/garage -O {desc['path']}")
        shared.exec(f"chmod +x {desc['path']}")
        shared.exec(f"{desc['path']} --version")

    for flav, desc in minio.items():
        if "version" not in desc: continue
        if exists(desc['path']): continue

        shared.exec(f"mkdir -p {shared.binary_path}")
        shared.exec(f"wget https://dl.min.io/server/minio/release/linux-amd64/archive/minio.RELEASE.{desc['version']} -O {desc['path']}")
        shared.exec(f"chmod +x {desc['path']}")
        shared.exec(f"{desc['path']} --version")

